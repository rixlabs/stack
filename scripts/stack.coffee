# Description:
#   File di personalitÃ  generale di faina
#
# Commands:
#   stack cosa ne pensi di java?
#

calls = ["Si?", "Cosa?", "Cosa c'Ã¨?", "Ma qual'Ã© il problema?", "Oooooo"]

greetings = ["Ciao raga"]

feelings = ["Bottoncione"]

opinions = ["Devo chiedere a Jelly..."]

overhear = ["Scusa non ti stavo ascoltando", "Pizzetta???"]

module.exports = (robot) ->

  robot.respond /cosa\s*(?:ne)?\s*pensi di (\w*)\?*/i, (msg) ->
    msg.send msg.random opinions

  robot.hear /(ciao|bye),?\s(.*)/i, (msg) ->
    if robot.name.toLowerCase() == msg.match[2].toLowerCase()
      msg.send msg.random greetings

  robot.respond /come (stai|va)\?*/, (msg) ->
    msg.send msg.random feelings

  robot.hear /(Ivan|ivan)/i, (msg) ->
    msg.send msg.random  overhear
    
  robot.hear /(ng|NG)/i, (msg) ->
    msg.send "New Generation?"
